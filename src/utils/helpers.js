
export const getLocalStorage = (name) => {
  return window.localStorage.getItem(name);
};

export const setLocalStorage = (name, data) => {
  window.localStorage.setItem(name, JSON.stringify(data));
};

export const removeLocalStorage = (name) => {
  window.localStorage.removeItem(name);
};
