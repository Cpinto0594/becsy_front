export const REQUEST_STATUSES = {
    NOT_LOADED: 'notLoaded',
    LOADING: 'loading',
    LOADED: 'loaded',
    FAILED: 'failed'
  };