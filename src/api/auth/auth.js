import api from './../api';

const baseUrl = 'auth';

export const logIn = (body) => {
  return api.post(`${baseUrl}/login`, body);
};