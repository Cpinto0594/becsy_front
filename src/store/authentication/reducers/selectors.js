export default function selectors(globalState) {
  const state = globalState.authentication;
  const getSession = () => state.session;
  const getLogInStatus = () => state.logInFetchStatus.status;
  const getLogInResponse = () => state.logInFetchStatus.payload?.message;

  return {
    getSession,
    getLogInStatus,
    getLogInResponse
  };
}
