import types from './types';

export const logIn = (payload) => ({
  type: types.LOGIN,
  payload
});

export const logInRequest = () => ({
  type: types.LOGIN_REQUEST
});

export const logInSuccess = (payload) => ({
  type: types.LOGIN_SUCCESS,
  payload
});

export const logInFailure = (payload) => ({
  type: types.LOGIN_FAILURE,
  payload
});