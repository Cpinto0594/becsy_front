import { createActionTypes } from "../../../utils/typesCreationHelper";

export default {
  ...createActionTypes('AUTHENTICATION', 'LOGIN')
};
