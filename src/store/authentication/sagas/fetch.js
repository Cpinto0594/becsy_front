import { call, put } from 'redux-saga/effects';
import { stopSubmit } from 'redux-form';
import * as actions from '../actions';
import { logIn } from '../../../api/auth/auth';
import { setLocalStorage } from '../../../utils/helpers';

export function* fetchLogin({ payload }) {
  try {
    yield put(actions.logInRequest());
    const session = yield call(logIn, payload);
    yield call(setLocalStorage, 'session', session);
    yield put(actions.logInSuccess(session));
  } catch (error) {
    const { status, statusText, data } = error;
    const { errors } = data;

    if (errors) yield put(stopSubmit('LoginForm', errors));

    yield put(
      actions.logInFailure({
        code: status,
        statusText,
        ...data
      })
    );
  }
}
