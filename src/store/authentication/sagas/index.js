import { takeLatest } from 'redux-saga/effects';
import types from '../actions/types';
import {
  fetchLogin
} from './fetch';

export default function* root() {
  yield takeLatest(types.LOGIN, fetchLogin);
}
